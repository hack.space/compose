# Compose for Hackathons.Space

This project contains docker-compose file for launching all services of the platform as they are designed [here](https://www.draw.io/?state=%7B%22ids%22:%5B%2214l28Npw_de82_4qbvYNiWy_YvraGxdHC%22%5D,%22action%22:%22open%22,%22userId%22:%22110291761691933908415%22%7D#G14l28Npw_de82_4qbvYNiWy_YvraGxdHC).

## Up - start all containers
`docker-compose up -d`  
Use -d for demon mode

## Down - stop all containers
`docker-compose down`

**For managing environment use and update relevant environment variables in .env file which should be located in the same folder as docker-compose file**

Example of .env:

```
CLIENT_UI_TAG=0.1.0
ADMIN_UI_TAG=0.1.0
CLIENT_API_TAG=0.1.0
ADMIN_API_TAG=0.1.0
BOT_MAILER_TAG=0.1.0
BOT_ASSIST_TAG=0.1.0

CLIENT_API_SECRET=secret
ADMIN_API_SECRET=secret
BOT_TOKEN=secret
DB_NAME=postgres
DB_USER=postgres
DB_PASSWORD=postgres

CLIENT_Ui_PORT_EXPOSE=5001
ADMIN_UI_PORT_EXPOSE=5002
CLIENT_API_PORT_EXPOSE=3000
ADMIN_API_PORT_EXPOSE=3001
```